<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('medicoes')->insert(
            [
                [

                    'pilha_bateria'    =>  'Pilha Alcalina Duracel AA',
                    'tensao_nominal'   =>  1.5,
                    'capacidade_corrente'  =>  2800,
                    'tensao_sem_carga'     => 1.304,
                    'tensao_com_carga'     => 1.286,
                    'resistencia_carga'   => 24.1,
                ],
                [
                
                    'pilha_bateria'    =>  'Bateria Unipower',
                    'tensao_nominal'   =>  12,
                    'capacidade_corrente'  =>  7000,
                    'tensao_sem_carga'     => 10.49,
                    'tensao_com_carga'     => 10.26,
                    'resistencia_carga'   => 24.1,
                ],
                [
                    'pilha_bateria'    =>  'pilha duracel AAA',
                    'tensao_nominal'   =>  1.5,
                    'capacidade_corrente'  =>  900,
                    'tensao_sem_carga'     => 0.987,
                    'tensao_com_carga'     => 0.866,
                    'resistencia_carga'   => 24.1,

                ],
                [
                    'pilha_bateria'    =>  'philips AAA',
                    'tensao_nominal'   =>  1.5,
                    'capacidade_corrente'  =>  1200,
                    'tensao_sem_carga'     => 371,
                    'tensao_com_carga'     => 1.312,
                    'resistencia_carga'   => 24.1,

                ],
                [
                    'pilha_bateria'    =>  'luatek',
                    'tensao_nominal'   =>  3.7,
                    'capacidade_corrente'  =>  1200,
                    'tensao_sem_carga'     => 2.516,
                    'tensao_com_carga'     => 2.515,
                    'resistencia_carga'   => 24.1,

                ],
                [
                    'pilha_bateria'    =>  'Energy Elgin',
                    'tensao_nominal'   =>  9,
                    'capacidade_corrente'  =>  250,
                    'tensao_sem_carga'     => 7.21,
                    'tensao_com_carga'     => 2.211,
                    'resistencia_carga'   => 24.1,

                ],
                [
                    'pilha_bateria'    =>  'Golite',
                    'tensao_nominal'   =>  9,
                    'capacidade_corrente'  =>  500,
                    'tensao_sem_carga'     => 5.90,
                    'tensao_com_carga'     => 0.27766,
                    'resistencia_carga'   => 24.1,

                ],
                [
                    'pilha_bateria'    =>  'JYX',
                    'tensao_nominal'   =>  3.7-4.2,
                    'capacidade_corrente'  =>  9800,
                    'tensao_sem_carga'     => 2.466,
                    'tensao_com_carga'     => 2.266,
                    'resistencia_carga'   => 24.1,

                ],
                [
                    'pilha_bateria'    =>  'Panasonic AA',
                    'tensao_nominal'   =>  1.5,
                    'capacidade_corrente'  =>  2550,
                    'tensao_sem_carga'     => 1.379,
                    'tensao_com_carga'     => 1.329,
                    'resistencia_carga'   => 24.1,

                ],
                [
                    'pilha_bateria'    =>  'Freedom',
                    'tensao_nominal'   =>  12,
                    'capacidade_corrente'  =>  30000,
                    'tensao_sem_carga'     => 10.68,
                    'tensao_com_carga'     => 10.63.,
                    'resistencia_carga'   => 24.1,

                ],
            ]

        );
    }
}
