@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Turma 2D2 - Grupo 3</h1>
        <h2>Participantes:</h2>
        <hr>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Matrícula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <td>0072541</td>
                <td>Nicolas Aniceto de Souza</td>
                <td>Gerente</td>
            </tr>
            <tr>
                <td>0072534</td>
                <td>Victor Simao P. F. T. de Aquino</td>
                <td>Desenvolvedor CSS</td>
            </tr>
            <tr>
                <td>0072560</td>
                <td>Vinicius Manoel Pedrosa</td>
                <td>Desenvolvedor HTML</td>
            </tr>
            <tr>
                <td>0072545</td>
                <td>Maria Eduarda Neves de Almeida</td>
                <td>Desenvolvedor PHP</td>
            </tr>
            <tr>
                <td>0073325</td>
                <td>Rayssa de Oliveira Mendes</td>
                <td>Banco de dados, imagem</td>
            </tr>
        </table>
        <img class="grupo" src="imgs/mira.png" alt="componentes do grupo">
    </main>

@endsection 

@section('rodape')
    <h4>Rodapé da página principal<h4>
@endsection
